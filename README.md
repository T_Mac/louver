postman is the service discovery element of the Pony Express set of tools. It leverages haproxy and etcd
to set up listeners at either a subdomain eg. service.example.com or by uri eg. example.com/uri. \
It is designed to work in tandem with clerk

### How it works
When running, the container will listen for trafic on port 80 and 443, requests are routed by the subdomain
that is used to access them or by the uri called. For example, a call to git.example.com would route the
request to the backend git, and so would a call to uncannypacket.com/git
The haproxy configuration is generated dynamically from ectd

Configuration Variables
HTTP_MODE       - Default 'https', 'http'    Optional
HTTPS_REDIRECT  - Default 'false', 'true'     Optional
PROXY_MODE      - Default 'subdomain', 'uri'  Optional
ETCD_HOST       - No Default                  Required
ETCD_PORT       - Default 2379                Optional
ETCD_ROOT       - Default '/stagecoach'       Optional
ETCD_CLIENT_AUTH - No Default          Optional

Certificates for incoming connections (frontend) should be mounted at '/etc/haproxy/certs.d/'
Client certificates for outbound connections (backend) should be mounted at /client-certs
Trusted CA bundles for verifying outbound (backend) ssl certs are read from /ca-bundle.pem

This image supports retrieving certificates for client authentication to backends from a vault server
First you must enable the feature by setting the env variable to any value.

ENABLE_LOCKSMITH='true' # This doesn't have to be any specific value

Then configure it using:
VAULT_TOKEN - valid auth token
VAULT_SERVER - host/ip of vault server eg http://127.0.0.1:8200
VAULT_ROLE - valid role for vault backend eg. cluster-member
VAULT_COMMON_NAME - common name to which to issue the certificate eg.

The below variables are set by default to point to the correct files/folders,
So unless you have a particular reason to - Don't mess with them -

CERT_PATH - directory where the retrieved certificates will be installed                       - set to '/client-certs'           
CA_CERT_PATH - path including all certs needed to verify VAULT_SERVER ssl                      - Not used
CA_CERT - path to ca cert need to verify VAULT_SERVER ssl - Takes precedence over CA_CERT_PATH - set to '/ca-bundle.pem'
