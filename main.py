#!/usr/bin/python
from jinja2 import Environment, FileSystemLoader
import os
from subprocess import call
import signal
import sys
import time
import smokesignal
from collections import defaultdict
from config import config, getLogger

moduleLogger = getLogger('app')
env = Environment(loader=FileSystemLoader('templates'), trim_blocks = True, lstrip_blocks = True)
POLL_TIMEOUT=5

signal.signal(signal.SIGCHLD, signal.SIG_IGN)

if config.proxy.https:
	moduleLogger.info('HTTPS ENABLED')

if config.proxy.https_redirect:
	moduleLogger.info('HTTPS REDIRECT ENABLED')

def get_etcd_config():
	etcd_config = {}

	if not config.etcd.host and not config.etcd.dns:
		moduleLogger.error('You must set either ETCD_HOST or ETCD_DNS!')
		sys.exit(1)

	etcd_config['port'] = config.etcd.host
	etcd_config['root'] = config.etcd.root

	if config.etcd.host:
		etcd_config['host'] = config.etcd.host
	if config.etcd.dns:
		etcd_config['dns'] = config.etcd.dns

	return etcd_config

def get_services(client):
	services = {}
	for service in client.services:
		services[service.uuid] = service.dump()

	return services

def generate_config(services):
	frontends = defaultdict(list)
	for id, service in services.items():
		if service['mode'] == 'http':
			frontends['http'] = True
		if service['mode'] == 'tcp':
			frontends['tcp'].append((id, service['service_port']))

	args = {
	'http_mode': config.proxy.https,
	'https_redirect': config.proxy.https_redirect,
	'services': services,
	'frontends': frontends
	}
	template = env.get_template('haproxy.cfg.tmpl')

	with open("/etc/haproxy/haproxy.cfg", "w") as f:
		f.write(template.render(**args))

def log_services(services):
	for id, config in services.items():
		moduleLogger.info('Found service: %s'%config['name'])
		for backend in config['backends']:
			moduleLogger.info('- Configured backend at %s'%backend['host'] + ' ' + 'srv_' + backend['id'])
		if len(config['backends']) == 0:
			moduleLogger.info('- No backends found for: %s'%config['name'])

if __name__ == "__main__":
	etcd_config = get_etcd_config()

	if etcd_config.get('dns', False):
		moduleLogger.info('Watching etcd at dns:%s for registered services.'%etcd_config['dns'])
	else:
		moduleLogger.info('Watching etcd at %s:%i for registered services.'%etcd_config['host'], etcd_config['port'])

	smoke_client = smokesignal.Client(**etcd_config)
	current_services = {}
	while True:
		try:
			moduleLogger.debug('Polling services...')
			services = get_services(smoke_client)
			if not services or services == current_services:
				time.sleep(POLL_TIMEOUT)
				continue

			log_services(services)
			generate_config(services)
			moduleLogger.info("config changed. Reloading haproxy")
			ret = call(["/app/reload-haproxy.sh"])
			if ret != 0:
				moduleLogger.info("reloading haproxy returned: ", ret)
				time.sleep(POLL_TIMEOUT)
				continue
			current_services = services

		except Exception as e:
			raise e
			moduleLogger.error("Error: %s"%str(e))

		time.sleep(POLL_TIMEOUT)
